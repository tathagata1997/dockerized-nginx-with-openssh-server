# Dockerized Nginx with OpenSSH Server



## Overview

This repository contains a Dockerfile and associated resources to build a Docker container that sets up an Nginx web server and an OpenSSH server. The container is designed to fulfill the specified requirements for serving web pages using Nginx, running on Ubuntu 22.04, and allowing key-based login for the root user via SSH.

## How to Build and Run

To build the Docker container, follow these steps:


- Clone the repository:

```
git clone https://gitlab.com/tathagata1997/dockerized-nginx-with-openssh-server.git

```

- Navigate to the project directory:

```
cd Dockerized Nginx with OpenSSH Server
```

- Build the Docker image:

```
docker build -t feature154.main -f Dockerfile.main.df .

```

- Run the Docker container:

```
docker run --rm --name server.local feature154.main /bin/sh

```
The Nginx and OpenSSH services will start automatically.

## Accessing the Services

### Nginx Web Server

Once the container is running, you can access the default Nginx welcome page by opening your browser and navigating to:

[http://server.local](http://server.local)

## OpenSSH Server

The OpenSSH server is configured to allow key-based login for the root user. To connect to the container via SSH, use the following command:

```
ssh -i feature154/tathagata.ghosh1.private_key.pem root@server.local

```
Please note that password-based SSH login for all users is denied for security reasons.

## Additional Information

Use the built-in continuous integration in GitLab.

- [Docker Documentation](https://docs.docker.com/get-started/)
- [Nginx Documentation](https://docs.nginx.com/nginx/)
- [OpenSSH Documentation](https://www.openssh.com/)
